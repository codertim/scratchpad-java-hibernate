package net.timwforce.dto;


import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
// @Entity(name="student")
@Table(name="student")
public class Student {
	@Id
	@Column(name="student_id")
	@GeneratedValue
	private int studentId;
	@Basic         // default, not needed
	private String name;
	@Transient     // transient or static to prevent creating field in table
	private String middleName;
	@Temporal (TemporalType.DATE)
	private Date myDate;
	@Temporal (TemporalType.TIME)
	private Date myTime;
	@Lob
	private String myLargeString;


	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

