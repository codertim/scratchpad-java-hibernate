package net.timwforce;


import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import java.util.List;

import net.timwforce.dto.Student;


/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
	{
		System.out.println( "Starting ..." );
		App app = new App();
		String testInput = args[0];
		Test test = Enum.valueOf(Test.class, testInput);

		if(test == Test.SIMPLE_LIST_QUERY) {
			app.doHibernateSimpleListQuery();
		} else if(test == Test.SIMPLE_CRITERIA) {
			app.doHibernateSimpleCriteria();
		} else if(test == Test.SIMPLE_DELETE) {
			app.doHibernateSimpleDelete();
		} else if(test == Test.SIMPLE_SAVE) {
			app.doHibernateSimpleSave();
		} else if(test == Test.SIMPLE_READ) {
			app.doHibernateSimpleRead();
		} else if(test == Test.HQL_READ1) {
			app.doHibernateHqlRead1();
		} else {
			System.out.println("Not a simple query !!!");
		}

		System.out.println( "\nDone.\n\n" );
	}


	private void doHibernateSimpleSave() {
		System.out.println("##### doHibernateSimpleSave - Starting ...");
		Student student = new Student();
		student.setName("John Doe");
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(student);
		session.getTransaction().commit();
		session.close();
	}


	private void doHibernateSimpleRead() {
		System.out.println("##### doHibernateSimpleRead - Starting ...");
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Student student = (Student) session.get(Student.class, 2);
		System.out.println("##### doHibernateSimpleRead - student read: " + student.getName());
		session.close();	
	}


	private void doHibernateSimpleCriteria() {
		System.out.println("##### doHibernateSimpleCriteria - Starting ...");
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Student.class);
		criteria.add(Restrictions.between("studentId", 30, 39));
		List<Student> students = (List<Student>) criteria.list();
		for(Student student : students) {
			System.out.println("##### doHibernateSimpleCriteria - Current student: " + student.getStudentId());
		}
		session.close();	
	}


	private void doHibernateSimpleDelete() {
		System.out.println("##### doHibernateSimpleDelete - Starting ...");
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Student student = new Student();
		student.setName("Greg");

		session.beginTransaction();
		session.save(student);
		System.out.println("##### doHibernateSimpleDelete - student saved: name=" + student.getName() + "   id=" + student.getStudentId());
		session.getTransaction().commit();

		session.beginTransaction();
		session.delete(student);
		session.getTransaction().commit();

		System.out.println("##### doHibernateSimpleDelete - student deleted: " + student.getName());
		session.close();	
	}


 	private void doHibernateSimpleListQuery() {
		System.out.println("##### doHibernateSimpleListQuery - Starting ...");

		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("select name from student");
		List<String> studentList = (List<String>) query.list();
		for(String student : studentList) {
			System.out.println("Current Student: " + student);
		}
		session.close();
	} 


	private void doHibernateHqlRead1() {
		System.out.println("##### doHibernateHqlRead1 - Starting ...");
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.createQuery("from Student where studentId > 10");
		List students = query.list();
		session.getTransaction().commit();
		System.out.println("Number of students: " + students.size());

		session.close();
	}

}


enum Test { HQL_READ1, SIMPLE_CRITERIA, SIMPLE_DELETE, SIMPLE_LIST_QUERY, SIMPLE_READ, SIMPLE_SAVE, NORMAL_QUERY}




